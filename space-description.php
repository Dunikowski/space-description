<?php
/**
 * Plugin Name: Space description
 * Description: Sekcja kodu html, dodawana z poziomu bloków Gutenberga
 * Version: 1.0
 * Author: Dawid Dunikowski
 **/


require_once dirname( __FILE__ ) .  '/classes/SpaceDescription.php';
require_once dirname( __FILE__ ) .  '/classes/SpaceACFOptions.php';

// Check if ACF is installed and activated
if ( ! function_exists( 'acf_add_options_page' ) ) {

  // ACF is not installed, so do not register options page
  add_action( 'admin_notices', 'my_acf_missing_notice' );

  function my_acf_missing_notice() {
    ?>
    <div class="error notice">
      <p><?php _e( 'Your plugin requires Advanced Custom Fields to be installed and activated.', 'my-text-domain' ); ?></p>
    </div>
    <?php
  }

  // Deactivate plugin if ACF is not installed
  deactivate_plugins( plugin_basename( __FILE__ ) );
  return;
}

// Initialize Space Description
$space_description = new SpaceDescription();
  
// Initialize Space ACF Options
$space_acf_options = new SpaceACFOptions();
