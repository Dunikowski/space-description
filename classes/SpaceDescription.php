<?php

class SpaceDescription
{

  public function __construct()
  {
    add_action('acf/init', array($this, 'acf_register_block_type'));
  }

  public function acf_register_block_type()
  {

    if (function_exists('acf_register_block_type')) {

      // register block.
      acf_register_block_type(array(
        'name'              => 'Space Description',
        'title'             => __('Space Description'),
        'description'       => __('Section kcode from Space project'),
        'render_template' => plugin_dir_path(__DIR__) . 'templates/space-description-template.php',
        'category' => 'space-description',
        'post_types' => array('post', 'page'),
        'enqueue_style' => plugin_dir_url(__FILE__) . '../css/space-description-style.css'
      ));
    }
  }

  public function add_block_category($categories, $post)
  {
    return array_merge(
      $categories,
      array(
        array(
          'slug' => 'space-description',
          'title' => __('Section Space Description Page', 'acf'),
          'icon' => 'smiley',
        ),
      )
    );
  }
}
