<?php

class SpaceACFOptions {
  private $options_page;

  public function __construct() {
      add_action( 'acf/init', array( $this, 'register_options_page' ) );
  }

  public function register_options_page() {
      if ( function_exists( 'acf_add_options_page' ) ) {
          $this->options_page = acf_add_options_page( array(
              'page_title' => __( 'Space Description Options', 'acf' ),
              'menu_title' => __( 'Space Description', 'acf' ),
              'menu_slug' => 'space-description-options',
              'capability' => 'edit_posts',
              'icon_url' => 'dashicons-star-filled',
              'position' => 50,
              'redirect' => false,
          ) );

          acf_add_local_field_group( array(
              'key' => 'space-description-fields',
              'title' => __( 'Space Description Section(fields)', 'acf' ),
              'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post',
                    ),
                ),
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ),
                ),
            ),
              'fields' => array(
                  array(
                      'key' => 'title',
                      'label' => __( 'Title', 'acf' ),
                      'name' => 'title',
                      'type' => 'text',
                  ),
                  array(
                      'key' => 'field_one',
                      'label' => __( 'Heading Text', 'acf' ),
                      'name' => 'field_one',
                      'type' => 'text',
                  ),
                  array(
                      'key' => 'field_two',
                      'label' => __( 'Paragraph', 'acf' ),
                      'name' => 'field_two',
                      'type' => 'textarea',
                  ),
                  array(
                      'key' => 'field_three',
                      'label' => __( 'Button Text', 'acf' ),
                      'name' => 'field_three',
                      'type' => 'text',
                  ),
                  array(
                      'key' => 'link',
                      'label' => __( 'Link', 'acf' ),
                      'name' => 'link',
                      'type' => 'url',
                  ),
              ),
          ) );
      }
  }
}