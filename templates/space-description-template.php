<?php
$spaceDescriptionData = new stdClass();
$spaceDescriptionData->title = get_field('title',$post_id);
$spaceDescriptionData->heading_text = get_field( 'field_one',$post_id);
$spaceDescriptionData->paragraph = get_field( 'field_two',$post_id);
$spaceDescriptionData->button_text = get_field( 'field_three',$post_id);
$spaceDescriptionData->link = get_field( 'link',$post_id);
?>

<section class="space-description" >
    <div class="space-description__left-column">
        <?php if ( $spaceDescriptionData->heading_text ) : ?>
            <p class="space-description__heading"><?php echo esc_html( $spaceDescriptionData->heading_text ); ?></p>
            <?php endif; ?>
            <?php if ( $spaceDescriptionData->title ) : ?>
                <h2 class="space-description__title"><?php echo esc_html( $spaceDescriptionData->title ); ?></h2>
            <?php endif; ?>
        <?php if ( $spaceDescriptionData->paragraph ) : ?>
            <div class="space-description__paragraph"><?php echo wp_kses_post( $spaceDescriptionData->paragraph ); ?></div>
        <?php endif; ?>
    </div>
    <?php if ( $spaceDescriptionData->button_text && $spaceDescriptionData->link ) : ?>
        <div class="space-description__right-column">
        <a href="<?php echo esc_url( $spaceDescriptionData->link ); ?>" class="space-description__link"><?php echo esc_html($spaceDescriptionData->button_text ); ?></a>
        </div>
        <?php elseif($spaceDescriptionData->link): ;?>
        <div class="space-description__right-column">
        <a href="<?php echo esc_url( $spaceDescriptionData->link ); ?>" class="space-description__link">EXPLORE</a>
        </div>
        <?php elseif($spaceDescriptionData->button_text):; ?>
        <div class="space-description__right-column">
        <a href="#" class="space-description__link"><?php echo esc_html( $spaceDescriptionData->button_text );?></a>
        </div>
        <?php else:;?>
        <div class="space-description__right-column">
            <a href="#" class="space-description__link">EXPLORE</a>
        </div>
        <?php endif; ?>

    </section>
